<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ExtractExcelRequest;
use Illuminate\Http\Request;
use App\Models\ExcelRecord;
use Excel;
use App\Jobs\ImportExcel;
/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

    public function index()
    {
        return view('backend.dashboard');
    }

    public function extract(Request $request)
    {
        ini_set('memory_limit', '-1');
        // $this->extract_data($request);
        $file_name  = $request->excel_file->getClientOriginalName();
        $excel_file = $request->excel_file->getRealPath();
        $uniq_id    = rand(100,999) . "-" . rand(10000, 99999);
        Excel::filter('chunk')->load($excel_file)->chunk(300, function($results) use ($file_name, $uniq_id)
        {   
            ImportExcel::dispatch($uniq_id, $file_name, $results);
        });

        // $datas = Model::all();
        // $count = 0;
        // foreach ($datas as $data) {
        //     $serialize = json_decode($data->content);
        //     $count += count($serialize);
        // }
        // dd($count);
        
        // $data = ExcelRecord::all();

      //   ini_set('memory_limit', '-1');
      // 	$data = Excel::load($request->excel_file, function($reader){
    		//     $reader->all();
    		// })->get();

      //   $sheetTitle = [];

      //   foreach($data as $sheet){
      //       array_push($sheetTitle,$sheet->getTitle());
      //   }

      //   $container = [ $sheetTitle, $data ];
        // dd('test');
        return json_encode("test");
    }

    public function extract_data($request)
    {
        $file_name  = $request->excel_file->getClientOriginalName();
        $excel_file = $request->file('excel_file')->getRealPath();
        $uniq_id    = rand(100,999) . "-" . rand(10000, 99999);
        Excel::filter('chunk')->load($excel_file)->chunk(250, function($results) use ($file_name, $uniq_id)
          {

              ExcelRecord::create([
                  'unique_id'    => $uniq_id,
                  'file_name'    => $file_name,
                  // 'sheet_name'   => "Sheet",
                  'chunk_record' => json_encode($results),
              ]);
          });

          return "test";
    }

    public function export(Request $request){

        // $export_excel = Excel::create('Filename', function($excel) {

        //     // Set the title
        //     $excel->setTitle('Our new awesome title');

        //     // Chain the setters
        //     $excel->setCreator('Maatwebsite')->setCompany('Maatwebsite');

        //     // Call them separately
        //     $excel->setDescription('A demonstration to change the file properties');

        //     $excel->sheet('sheetName', function($sheet) {
        //         $sheet->row(1, array(
        //              'test1', 'test2'
        //         ));
        //     });
        // })->export('xls');


      // Exporting Large Files
        // $orders = Order::withTrashed()->where('status', 'done');
        // Excel::create('Report', function($excel) use ($orders) {
        //     $excel->sheet('report', function($sheet) use($orders) {
        //         $sheet->appendRow(array(
        //             'id', 'landing', 'departure', 'phone_id'
        //         ));
        //         $orders->chunk(100, function($rows) use ($sheet)
        //         {
        //             foreach ($rows as $row)
        //             {
        //                 $sheet->appendRow(array(
        //                     $row->id, $row->landing, $row->departure, $row->phone_id
        //                 ));
        //             }
        //         });
        //     });
        // })->download('xlsx');
      // 

        $myFile = Excel::create("filename", function($excel) {
           $excel->setTitle('title');
            $excel->sheet('sheetName', function($sheet) {
                $sheet->row(1, array(
                     'test1', 'test2'
                ));
            });
        });

        $myFile = $myFile->string('xlsx'); //change xlsx for the format you want, default is xls
        $response =  array(
           'name' => "filename", //no extention needed
           'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile) //mime type of used format
        );

        return response()->json($response);
    }

}
