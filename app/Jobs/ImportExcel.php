<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\ExcelRecord;

class ImportExcel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $uniq_id;

    protected $results;

    protected $file_name;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($uniq_id, $file_name, $results)
    {
        $this->uniq_id = $uniq_id;
        $this->results = $results;
        $this->file_name = $file_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $query = ExcelRecord::create([
            'unique_id'    => $this->uniq_id,
            'file_name'    => $this->file_name,
            'sheet_name'   => "Sheet",
            'chunk_record' => $this->results,
        ]);
        dd($query);
    }
}
