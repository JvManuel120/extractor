<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::post('dashboard/extract', 'DashboardController@extract')->name('dashboard.extract');
Route::post('dashboard/export', 'DashboardController@export')->name('dashboard.export');
