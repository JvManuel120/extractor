@extends('backend.layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>Extractor Form</strong>
                </div><!--card-header-->
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-10">
                            <form id="form-extractor" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <label>Upload Excel</label>
                                        <input type="file" id="excel_file"  name="excel_file" class="form-control">
                                    </div>
                                    <div class="col-sm-2">
                                        <center>
                                            <button class="btn btn-primary btn-extract">Extract</button>
                                        </center>
                                    </div>
                                </div>
                            </form>
                        </div>
                        {{-- <div class="col-sm-3">
                            <label>Select Sheet</label>
                            <select id="sheet_tab" class="form-control"></select>
                            <br>
                        </div> --}}
                        <div class="col-sm-1">
                            <button id="btn-validate" class="btn btn-warning btn-validate">Validate</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btn-export" class="btn btn-success btn-export">Export</button>
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-12 table-responsive" id="table_container">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-styles')
    <style type="text/css">
        .excel_content thead tr th{
            text-transform: uppercase;
            background-color: #f2f2f2;
        }
        .col-number{
            background-color: #f2f2f2;
            text-align: center;
            font-weight: bold;
        }
        .dataTables_wrapper{
            display: none;
        }
        .dataTables_wrapper.active{
            display: block !important;
        }
        label{
            font-weight: bold;
        }
        .btn-extract, .btn-validate, .btn-export{
            margin-top: 31px;
            color: white !important;
        }
    </style>
@endpush

@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $(document).on('click', '#btn-validate', function(){
                $('.excel_content').each(function(i){
                    $(this).DataTable().rows().iterator('row', function(context, index){
                        var node = $(this.row(index).node());
                        var data = $(this.row(index).data()); 
                        console.log(data);
                        $(node).find('td').each(function(i){
                            var td = $(this);
                            $(td).removeClass('table-danger');
                            if(validate(td.text().toLowerCase(), td.attr('data-field'))){
                                // $(td).addClass('table-success');
                            }
                            else{
                                $(td).addClass('table-danger');
                            }
                        });
                    });
                });
            });

            function validate(data, field){

                var boolean      = true;
                var characters   = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;

                if(data == ""){
                    boolean = false;
                }

                switch(field) {

                    case "lname":

                        var rules  = ['jr', 'sr', '.', 'III', 'II', 'IV', 'VI'];

                        $.each(rules, function(count, rule){
                            if(data.includes(rule))
                            {
                                boolean = false;
                            }
                        });
                
                        break;

                    case "mname":

                        if(data.length == 1 || characters.test(data)){
                            boolean = false;
                        }

                        break;

                    default:
                        
                }

                return boolean;

            }

            $(document).on('change', '#sheet_tab', function(){
                var selected = $(this).val();
                $('.dataTables_wrapper').removeClass('active');
                $('#' + selected + "_wrapper").addClass('active');
            });

            $(document).on('click', '#btn-export', function(){

                var data = null;

                $.ajax({
                    url  : '{{ route('admin.dashboard.export') }}',
                    type : 'POST',
                    data: data,
                    beforeSend : function(xhr){
                        swal({
                          title: 'Exporting',
                          text: 'This will take a while .. Please Wait ..',
                          onOpen: () => {
                            swal.showLoading()
                          }
                        });
                    },
                    success : function(response){
                        var a = document.createElement("a");
                        a.href = response.file; 
                        a.download = response.name;
                        document.body.appendChild(a);
                        a.click();
                        a.remove();
                        swal("Success!", "Excel File has been exported", "success");
                    },
                    error: function(xhr, status, error) 
                    {
                        swal({
                          type: 'error',
                          title: 'Oops...',
                          text: error
                        });
                    }  
                });

            });

            $("#form-extractor").submit(function(e){
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url  : '{{ route('admin.dashboard.extract') }}',
                    type : 'POST',
                    dataType: 'json',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend : function(xhr){
                        swal({
                          title: 'Extracting',
                          text: 'This will take a while .. Please Wait ..',
                          onOpen: () => {
                            swal.showLoading()
                          }
                        });
                    },
                    success : function(data){
                        // console.log(data);

                        // $.each(data, function(count_data, data_value){

                        //     var record = $.parseJSON(data[count_data]['chunk_record']);

                        //     $.each(record, function(count_record, value){

                        //         var keys = Object.keys(value);

                        //         if(count_data == 0)
                        //         {
                        //             $('#sheet thead tr').append(`
                        //                 <th>No</th>
                        //             `);
                        //             $.each(keys, function(count_key, key){
                        //                 $('#sheet thead tr').append(`
                        //                     <th>` + key + `</th>
                        //                 `);
                        //             });
                        //         }

                        //         var row  = `<tr>`;
                        //         $.each(keys, function(count_key, key){

                        //             if(count_key == 0){
                        //                 row += `<td class="col-number" data-field="col-number">` + ( count_record + 1 ) + `</td>`;
                        //             }

                        //             var new_value = value[key] != null ? value[key] : "";
                        //             row += `<td data-field="` + key + `" contenteditable>` + new_value + `</td>`;
                        //         });
                        //         row += `</tr>`;
                        //         $('#sheet tbody').append(row);

                        //     });

                        // });

                        // $.each(data, function(row){

                        //     var sheet_name = data[row]['sheet_name'].replace(" ", "-");
                        //     var record     = data[row]['chunk_record'];

                        //     if($('.' + sheet_name).length == 0)
                        //     {
                        //         $('#sheet_tab').append(`
                        //             <option value="` + sheet_name + `"> ` + sheet_name + ` </option>
                        //         `);
                        //         $('#table_container').append(`
                        //             <table id="` + sheet_name + `" class="table excel_content ` + sheet_name + `">
                        //                 <thead>
                        //                     <tr></tr>
                        //                 </thead>
                        //                 <tbody></tbody>
                        //             </table>
                        //         `);
                        //     }
                            
                        //     var record_json = $.parseJSON(record);

                        //     $('#'+ sheet_name +' thead tr').append(`
                        //             <th>No</th>
                        //     `);

                        //     $.each(record_json, function(count_record, value){

                        //         var keys = Object.keys(value);

                        //         if(count_record == 0)
                        //         {
                        //             $.each(keys, function(count_key, key){
                        //                 $('#'+ sheet_name +' thead tr').append(`
                        //                         <th>` + key + `</th>
                        //                     `);
                        //             });
                        //         }

                        //         var row  = `<tr>`;
                        //         $.each(keys, function(count_key, key){

                        //             if(count_key == 0){
                        //                 row += `<td class="col-number" data-field="col-number">` + ( count_record + 1 ) + `</td>`;
                        //             }

                        //             var new_value = value[key] != null ? value[key] : "";
                        //             row += `<td data-field="` + key + `" contenteditable>` + new_value + `</td>`;
                        //         });
                        //         row += `</tr>`;
                        //         $('#' + sheet_name + ' tbody').append(row);

                        //     });
                        //     $('#' + sheet_name ).DataTable();

                        // });

                        // $('.dataTables_wrapper:first-child').addClass('active');
                        
                        // 

                        // var sheets = ['sheet'];

                        // $.each(sheets, function(count_sheet, sheet){

                        //     var keys   = Object.keys(data[0]);

                        //     // $('#sheet_tab').append(`
                        //     //         <option value="` + count_sheet + `"> ` + sheet + ` </option>
                        //     //     `);
                            
                        //     $('#table_container').append(`
                        //         <table id="` + count_sheet + `" class="table excel_content">
                        //             <thead>
                        //                 <tr></tr>
                        //             </thead>
                        //             <tbody></tbody>
                        //         </table>
                        //     `);

                        //     $('#'+ count_sheet +' thead tr').append(`
                        //             <th>No</th>
                        //         `);


                        //     $.each(keys, function(count_key, key){
                        //         $('#'+ count_sheet +' thead tr').append(`
                        //                 <th>` + key + `</th>
                        //             `);
                        //     });

                        //     $.each(data, function(count_value, value){
                        //         var row  = `<tr>`;
                        //         $.each(keys, function(count_key, key){

                        //             if(count_key == 0){
                        //                 row += `<td class="col-number" data-field="col-number">` + ( count_value + 1 ) + `</td>`;
                        //             }

                        //             var new_value = value[key] != null ? value[key] : "";
                        //             row += `<td data-field="` + key + `" contenteditable>` + new_value + `</td>`;
                        //         });
                        //         row += `</tr>`;
                        //         $('#' + count_sheet + ' tbody').append(row);
                        //     });

                        //     $('#' + count_sheet).DataTable();

                        // });

                        // $('.dataTables_wrapper:first-child').addClass('active');

                        swal("Success!", "File has been extracted", "success");
                    },
                    error: function(xhr, status, error) 
                    {
                        swal({
                          type: 'error',
                          title: 'Oops...',
                          text: error
                        });
                    }  
                });
            });
        });
    </script>
@endpush